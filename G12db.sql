-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 02-01-2016 a las 11:17:30
-- Versión del servidor: 5.5.46-0ubuntu0.14.04.2
-- Versión de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `G12db`
--
CREATE SCHEMA IF NOT EXISTS `G12db` DEFAULT CHARACTER SET utf8;
USE `G12db`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Codes`
--

CREATE TABLE IF NOT EXISTS `Codes` (
  `Code` varchar(30) NOT NULL,
  `Popular_idPopular` int(11) NOT NULL,
  `Pincho_code` int(11) NOT NULL,
  `Pincho_Establishment_idEstablishment` int(11) NOT NULL,
  `wasUsed` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`Code`,`Popular_idPopular`,`Pincho_code`,`Pincho_Establishment_idEstablishment`),
  KEY `fk_Codes_Popular1_idx` (`Popular_idPopular`),
  KEY `fk_Codes_Pincho1_idx` (`Pincho_code`,`Pincho_Establishment_idEstablishment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Codes`
--

INSERT INTO `Codes` (`Code`, `Popular_idPopular`, `Pincho_code`, `Pincho_Establishment_idEstablishment`, `wasUsed`) VALUES
('87o2upel6t', 11, 1, 1, '1'),
('c9hqfr5cjb', 11, 5, 5, '1'),
('e117vvg88i', 11, 4, 2, '1'),
('q7l7wooa9w', 14, 1, 1, '1'),
('tazzsy00dn', 14, 5, 5, '1'),
('yj8hnfo30l', 14, 4, 2, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Comments`
--

CREATE TABLE IF NOT EXISTS `Comments` (
  `idComment` int(11) NOT NULL AUTO_INCREMENT,
  `Message` varchar(300) DEFAULT NULL,
  `Popular_idPopular` int(11) NOT NULL,
  `Pincho_code` int(11) NOT NULL,
  `Pincho_Establishment_idEstablishment` int(11) NOT NULL,
  PRIMARY KEY (`idComment`,`Popular_idPopular`,`Pincho_code`,`Pincho_Establishment_idEstablishment`),
  KEY `fk_PopularValorations_Popular1_idx` (`Popular_idPopular`),
  KEY `fk_PopularValorations_Pincho1_idx` (`Pincho_code`,`Pincho_Establishment_idEstablishment`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `Comments`
--

INSERT INTO `Comments` (`idComment`, `Message`, `Popular_idPopular`, `Pincho_code`, `Pincho_Establishment_idEstablishment`) VALUES
(1, 'Fue un buen pincho.			    ', 11, 1, 1),
(2, 'Estoy de acuerdo.			    ', 14, 1, 1),
(3, 'Pues claro.', 11, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Establishment`
--

CREATE TABLE IF NOT EXISTS `Establishment` (
  `idEstablishment` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `coordenates` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `schedule` varchar(45) DEFAULT NULL,
  `webpage` varchar(45) DEFAULT NULL,
  `password` varchar(45) NOT NULL,
  `isValidated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idEstablishment`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Establishment`
--

INSERT INTO `Establishment` (`idEstablishment`, `name`, `coordenates`, `phone`, `email`, `schedule`, `webpage`, `password`, `isValidated`) VALUES
(1, 'Bar1', '42°21''05.3"N 7°52''09.6"W', '655656565', 'manolo@gmail.com', '10:00-02:00', 'www.barmanolo.com', 'Bar1', 1),
(2, 'Bar2', '42°20''55.9"N 7°52''30.7"W', '988444444', 'lola@gmail.com', '10:00-12:00', 'www.lola.com', 'Bar2', 1),
(3, 'Bar3', '42°21''58.3"N 7°52''26.7"W', '685561018', 'kurdakri@gmail.com', '10:00-22:00', 'www.marca.com', 'Bar3', NULL),
(4, 'Bar4', '42°20''32.5"N 7°51''19.9"W', '666666666', 'barcalamar@hotmail.com', '16:00-04:00', 'www.barcalamar.com', 'Bar4', 1),
(5, 'Bar5', '42°20''25.8"N 7°51''55.6"W', '988332120', 'oantro@gmail.com', '12:00-22:00', 'www.oantro.com', 'Bar5', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Organizer`
--

CREATE TABLE IF NOT EXISTS `Organizer` (
  `idOrganizer` int(11) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idOrganizer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Organizer`
--

INSERT INTO `Organizer` (`idOrganizer`, `password`, `name`, `phone`, `email`) VALUES
(0, 'admin', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Pincho`
--

CREATE TABLE IF NOT EXISTS `Pincho` (
  `code` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `ingredients` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `isValidated` varchar(45) DEFAULT NULL,
  `Establishment_idEstablishment` int(11) NOT NULL,
  `isFinalist` varchar(8) DEFAULT NULL,
  `isWinner` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`code`,`Establishment_idEstablishment`),
  KEY `fk_Pincho_Establishment_idx` (`Establishment_idEstablishment`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Pincho`
--

INSERT INTO `Pincho` (`code`, `name`, `price`, `description`, `ingredients`, `url`, `isValidated`, `Establishment_idEstablishment`, `isFinalist`, `isWinner`) VALUES
(1, 'Pincho1', '3$', 'Pincho de tortilla', 'Tortilla,pan.', 'http://i.imgur.com/9HNAgsL.jpg', '1', 1, NULL, '0'),
(3, 'Pincho3', '6$', 'Plato de canapés.', 'Pan;Gambas;Caviar;Tortilla;Salsas', 'http://i.imgur.com/lQGp36j.jpg', '1', 3, NULL, '0'),
(4, 'Pincho2', '2$', 'Patatas Bravas con Salsa Picante', 'Patatas;Salsa Picante.', 'http://i.imgur.com/Fsugm.jpg', '1', 2, NULL, '0'),
(5, 'Pincho5', '5$', 'Revuelto de Carne', 'Carne de Ternera;Chorizo;Pimiento', 'http://i.imgur.com/n454VCh.jpg', '1', 5, NULL, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Popular`
--

CREATE TABLE IF NOT EXISTS `Popular` (
  `idPopular` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idPopular`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `Popular`
--

INSERT INTO `Popular` (`idPopular`, `name`, `password`, `phone`, `email`) VALUES
(1, 'dsaf', '12312', 'adsf', 'sadf@dasfa'),
(4, 'rere', '123123', 'asdfa', 'sadf@dasfa'),
(8, 'federico', 'federico', '988733322', 'federico@gmail.com'),
(9, 'hola', 'hola', '989898', 'root@mail'),
(11, 'mario', 'mario', '666666666', 'marioyb2@hotmail.com'),
(12, 'cris', 'cris', '677676767', 'cris@gmail.com'),
(13, 'alejandro', 'alejandro', '699696969', 'alejandro@gmail.com'),
(14, 'homer', 'homer', '988777766', 'homer@homer.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PopularValorations`
--

CREATE TABLE IF NOT EXISTS `PopularValorations` (
  `idValoration` int(11) NOT NULL AUTO_INCREMENT,
  `Like` tinyint(1) DEFAULT NULL,
  `Popular_idPopular` int(11) NOT NULL,
  `Pincho_code` int(11) NOT NULL,
  `Pincho_Establishment_idEstablishment` int(11) NOT NULL,
  PRIMARY KEY (`idValoration`,`Popular_idPopular`,`Pincho_code`,`Pincho_Establishment_idEstablishment`),
  KEY `fk_PopularValorations_Popular1_idx` (`Popular_idPopular`),
  KEY `fk_PopularValorations_Pincho1_idx` (`Pincho_code`,`Pincho_Establishment_idEstablishment`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `PopularValorations`
--

INSERT INTO `PopularValorations` (`idValoration`, `Like`, `Popular_idPopular`, `Pincho_code`, `Pincho_Establishment_idEstablishment`) VALUES
(4, NULL, 8, 4, 2),
(5, NULL, 8, 1, 1),
(6, NULL, 8, 1, 1),
(7, NULL, 14, 5, 5),
(8, NULL, 11, 5, 5),
(9, NULL, 11, 4, 2),
(10, NULL, 11, 5, 5),
(11, NULL, 11, 1, 1),
(12, NULL, 14, 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Professional`
--

CREATE TABLE IF NOT EXISTS `Professional` (
  `idProfessional` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idProfessional`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `Professional`
--

INSERT INTO `Professional` (`idProfessional`, `name`, `password`, `email`, `phone`) VALUES
(1, 'Pro1', 'pro1', 'pro1@email.com', '988334433'),
(3, 'Pro2', 'pro2', 'pro2@email.com', '988336633'),
(4, 'Pro3', 'pro3', 'pro3@gmail.com', '688776677');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ProfessionalValorations`
--

CREATE TABLE IF NOT EXISTS `ProfessionalValorations` (
  `idProfessionalValorations` int(11) NOT NULL AUTO_INCREMENT,
  `Calification` int(11) DEFAULT NULL,
  `Professional_idProfessional` int(11) NOT NULL,
  `Pincho_code` int(11) NOT NULL,
  `Pincho_Establishment_idEstablishment` int(11) NOT NULL,
  PRIMARY KEY (`idProfessionalValorations`,`Professional_idProfessional`,`Pincho_code`,`Pincho_Establishment_idEstablishment`),
  KEY `fk_ProfessionalValorations_Professional1_idx` (`Professional_idProfessional`),
  KEY `fk_ProfessionalValorations_Pincho1_idx` (`Pincho_code`,`Pincho_Establishment_idEstablishment`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `ProfessionalValorations`
--

INSERT INTO `ProfessionalValorations` (`idProfessionalValorations`, `Calification`, `Professional_idProfessional`, `Pincho_code`, `Pincho_Establishment_idEstablishment`) VALUES
(1, 4, 1, 1, 1),
(2, 1, 1, 4, 2),
(3, 3, 3, 5, 5),
(4, 5, 3, 3, 3);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Codes`
--
ALTER TABLE `Codes`
  ADD CONSTRAINT `fk_Codes_Pincho1` FOREIGN KEY (`Pincho_code`, `Pincho_Establishment_idEstablishment`) REFERENCES `Pincho` (`code`, `Establishment_idEstablishment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Codes_Popular1` FOREIGN KEY (`Popular_idPopular`) REFERENCES `Popular` (`idPopular`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Pincho`
--
ALTER TABLE `Pincho`
  ADD CONSTRAINT `fk_Pincho_Establishment` FOREIGN KEY (`Establishment_idEstablishment`) REFERENCES `Establishment` (`idEstablishment`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `PopularValorations`
--
ALTER TABLE `PopularValorations`
  ADD CONSTRAINT `fk_PopularValorations_Pincho1` FOREIGN KEY (`Pincho_code`, `Pincho_Establishment_idEstablishment`) REFERENCES `Pincho` (`code`, `Establishment_idEstablishment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_PopularValorations_Popular1` FOREIGN KEY (`Popular_idPopular`) REFERENCES `Popular` (`idPopular`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ProfessionalValorations`
--
ALTER TABLE `ProfessionalValorations`
  ADD CONSTRAINT `fk_ProfessionalValorations_Pincho1` FOREIGN KEY (`Pincho_code`, `Pincho_Establishment_idEstablishment`) REFERENCES `Pincho` (`code`, `Establishment_idEstablishment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ProfessionalValorations_Professional1` FOREIGN KEY (`Professional_idProfessional`) REFERENCES `Professional` (`idProfessional`) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE USER 'G12user' IDENTIFIED BY 'G12pass';
GRANT ALL ON `G12db`.* TO 'G12user';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
