-- MySQL Script generated by MySQL Workbench
-- jue 12 nov 2015 12:11:56 CET
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Pinchos
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Pinchos
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Pinchos` DEFAULT CHARACTER SET utf8 ;
USE `Pinchos` ;

-- -----------------------------------------------------
-- Table `Pinchos`.`Professional`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pinchos`.`Professional` ;

CREATE TABLE IF NOT EXISTS `Pinchos`.`Professional` (
  `idProfessional` INT NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` INT NOT NULL,
  PRIMARY KEY (`idProfessional`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Pinchos`.`Pincho`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pinchos`.`Pincho` ;

CREATE TABLE IF NOT EXISTS `Pinchos`.`Pincho` (
  `code` INT NOT NULL,
  `url` VARCHAR(45) NULL,
  `Professional_idProfessional` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `price` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `ingredients` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`code`, `Professional_idProfessional`),
  INDEX `fk_Pincho_Professional1_idx` (`Professional_idProfessional` ASC),
  CONSTRAINT `fk_Pincho_Professional1`
    FOREIGN KEY (`Professional_idProfessional`)
    REFERENCES `Pinchos`.`Professional` (`idProfessional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Pinchos`.`Establishment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pinchos`.`Establishment` ;

CREATE TABLE IF NOT EXISTS `Pinchos`.`Establishment` (
  `idEstablishment` INT NOT NULL,
  `coordenates` VARCHAR(45) NULL,
  `Pincho_code` INT NOT NULL,
  `Pincho_Professional_idProfessional` INT NOT NULL,
  `isValidated` TINYINT(1) NULL,
  `phone` VARCHAR(11) NOT NULL,
  `emaill` VARCHAR(45) NOT NULL,
  `schedule` VARCHAR(30) NULL,
  `webpage` VARCHAR(50) NULL,
  PRIMARY KEY (`idEstablishment`, `Pincho_code`, `Pincho_Professional_idProfessional`),
  INDEX `fk_Establishment_Pincho1_idx` (`Pincho_code` ASC, `Pincho_Professional_idProfessional` ASC),
  CONSTRAINT `fk_Establishment_Pincho1`
    FOREIGN KEY (`Pincho_code` , `Pincho_Professional_idProfessional`)
    REFERENCES `Pinchos`.`Pincho` (`code` , `Professional_idProfessional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Pinchos`.`Organizer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pinchos`.`Organizer` ;

CREATE TABLE IF NOT EXISTS `Pinchos`.`Organizer` (
  `idOrganizer` INT NOT NULL,
  `phone` INT NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `Professional_idProfessional` INT NOT NULL,
  `Professional_Organizer_idOrganizer` INT NOT NULL,
  `Establishment_idEstablishment` INT NOT NULL,
  `Establishment_Pincho_code` INT NOT NULL,
  `Establishment_Pincho_Professional_idProfessional` INT NOT NULL,
  `Establishment_Pincho_Professional_Organizer_idOrganizer` INT NOT NULL,
  PRIMARY KEY (`idOrganizer`, `Professional_idProfessional`, `Professional_Organizer_idOrganizer`, `Establishment_idEstablishment`, `Establishment_Pincho_code`, `Establishment_Pincho_Professional_idProfessional`, `Establishment_Pincho_Professional_Organizer_idOrganizer`),
  INDEX `fk_Organizer_Professional1_idx` (`Professional_idProfessional` ASC, `Professional_Organizer_idOrganizer` ASC),
  INDEX `fk_Organizer_Establishment1_idx` (`Establishment_idEstablishment` ASC, `Establishment_Pincho_code` ASC, `Establishment_Pincho_Professional_idProfessional` ASC, `Establishment_Pincho_Professional_Organizer_idOrganizer` ASC),
  CONSTRAINT `fk_Organizer_Professional1`
    FOREIGN KEY (`Professional_idProfessional`)
    REFERENCES `Pinchos`.`Professional` (`idProfessional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Organizer_Establishment1`
    FOREIGN KEY (`Establishment_idEstablishment` , `Establishment_Pincho_code` , `Establishment_Pincho_Professional_idProfessional`)
    REFERENCES `Pinchos`.`Establishment` (`idEstablishment` , `Pincho_code` , `Pincho_Professional_idProfessional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Pinchos`.`Popular`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pinchos`.`Popular` ;

CREATE TABLE IF NOT EXISTS `Pinchos`.`Popular` (
  `idPopular` INT NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`idPopular`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Pinchos`.`PopularValorations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pinchos`.`PopularValorations` ;

CREATE TABLE IF NOT EXISTS `Pinchos`.`PopularValorations` (
  `Popular_idPopular` INT NOT NULL,
  `Pincho_code` INT NOT NULL,
  `Pincho_Professional_idProfessional` INT NOT NULL,
  `Like` INT NULL,
  PRIMARY KEY (`Popular_idPopular`, `Pincho_code`, `Pincho_Professional_idProfessional`),
  INDEX `fk_Popular_has_Pincho_Pincho1_idx` (`Pincho_code` ASC, `Pincho_Professional_idProfessional` ASC),
  INDEX `fk_Popular_has_Pincho_Popular1_idx` (`Popular_idPopular` ASC),
  CONSTRAINT `fk_Popular_has_Pincho_Popular1`
    FOREIGN KEY (`Popular_idPopular`)
    REFERENCES `Pinchos`.`Popular` (`idPopular`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Popular_has_Pincho_Pincho1`
    FOREIGN KEY (`Pincho_code` , `Pincho_Professional_idProfessional`)
    REFERENCES `Pinchos`.`Pincho` (`code` , `Professional_idProfessional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Pinchos`.`ProfessionalValoration`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Pinchos`.`ProfessionalValoration` ;

CREATE TABLE IF NOT EXISTS `Pinchos`.`ProfessionalValoration` (
  `Professional_idProfessional` INT NOT NULL,
  `Professional_Organizer_idOrganizer` INT NOT NULL,
  `Pincho_code` INT NOT NULL,
  `Pincho_Professional_idProfessional` INT NOT NULL,
  `Calification` INT NOT NULL,
  PRIMARY KEY (`Professional_idProfessional`, `Professional_Organizer_idOrganizer`, `Pincho_code`, `Pincho_Professional_idProfessional`),
  INDEX `fk_Professional_has_Pincho_Pincho1_idx` (`Pincho_code` ASC, `Pincho_Professional_idProfessional` ASC),
  INDEX `fk_Professional_has_Pincho_Professional1_idx` (`Professional_idProfessional` ASC, `Professional_Organizer_idOrganizer` ASC),
  CONSTRAINT `fk_Professional_has_Pincho_Professional1`
    FOREIGN KEY (`Professional_idProfessional`)
    REFERENCES `Pinchos`.`Professional` (`idProfessional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Professional_has_Pincho_Pincho1`
    FOREIGN KEY (`Pincho_code` , `Pincho_Professional_idProfessional`)
    REFERENCES `Pinchos`.`Pincho` (`code` , `Professional_idProfessional`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SET SQL_MODE = '';
GRANT USAGE ON *.* TO admin;
 DROP USER admin;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'admin' IDENTIFIED BY 'admin';

GRANT ALL ON `Pinchos`.* TO 'admin';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
