<?php
session_start();
if($_SESSION["validated"] != "Professional"){
    header("Location: signIn.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Professional Jury Homepage</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/heroic-features.css" rel="stylesheet">
    <link href="../css/customProfessionalVote.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<<<<<<< HEAD
        <![endif]-->

    </head>


    <body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Menu</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../views/homeProfessionalJury.php">Professional Jury Home</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="professionalProfile.php">My profile</a>
                    </li>
                     <li>
                        <a href="../controllers/professionalController.php?action=vote">Vote to pinchos</a>
                    </li>
                    <li>
                        <a href="../controllers/professionalController.php?action=finalist">Select Finalist</a>
                    </li>
		    <li>
                       <a href="../controllers/professionalController.php?action=logout">LogOut</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
     <h1>Pinchos Finalist Selection</h1>

     <div style="float:left">
         <?php
         if(isset($_GET["msg"])){
             $msg = $_GET["msg"];
             echo($msg);
         }
         if(isset($_GET["pinchos"])){
             $pinchos = unserialize($_GET["pinchos"]);
             foreach($pinchos as $pincho){
                $id = $pincho[0]["code"];
                $name = $pincho[0]["name"];
                $price = $pincho[0]["price"];
                $description = $pincho[0]["description"];
                $ingredients = $pincho[0]["ingredients"];

                echo"<div>";
                $url = $pincho[0]["url"];
                echo "<h4>NAME: $name</h4>";
                echo "<h4>PRICE: $price</h4>";
                echo "<h4>DESCRIPTION: $description</h4>";
                echo "<h4>INGREDIENTS: $ingredients</h4>";
                echo"</div>";
                echo"<div>";
                echo "<h4>IMAGE:<h4><img src='".$url."' width='200' height='200'>";
                echo "<br>";
                echo "<br>";
                echo "<form action='../controllers/professionalController.php' method='POST' id='finalist'>";
                echo "<input type='hidden' name='idFinalist' value='$id'>";
                echo "<br>";
                echo "<button type='submit' class='btn btn-primary'>Select this as Finalist</button>";
                echo "</form>";
                echo"</div>";

                echo "<br>";
            }
        }
        ?>
</div>
<!-- /.container -->

<!-- jQuery -->
<script src="../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>

</body>

</html>
