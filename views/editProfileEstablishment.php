<?php
session_start();
if($_SESSION["validated"] != "Establishment"){
header("Location: signIn.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ABP-PROYECT</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/heroic-features.css" rel="stylesheet">
    <link href="../css/customEditProfileEstablishment.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../controllers/establishmentController.php?action=dataget">Establishment</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="editProfileEstablishment.php">Edit Profile</a>
                    </li>
                    <li>
                        <a href="requestManagement.php">Request Management</a>
                    </li>
                    <li>
                        <a href="../controllers/pinchoController.php?action=view">View Pincho</a>
                    </li>
		    <li>
			<a href="../controllers/establishmentController.php?action=code">Code Generation</a>
		    </li>
		    <li>
			<a href="../controllers/establishmentController.php?action=logout">LogOut</a>
		    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>



    <!-- Registration form -->
    <section id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h1>Edit profile page</h1>
		    
			<form role="form" action="../controllers/establishmentController.php" method="POST" style="text-align:left">
			  <div class="form-group">
			    <label>Name</label>
			    <input required type="text" name="name" class="form-control" id="ejemplo_email_1"
				   placeholder="Name of the establishment">
			  </div>
			  <div class="form-group">
			    <label>Coordenates</label>
			    <input required type="text" name="coordenates" class="form-control" id="ejemplo_email_1"
				   placeholder="Coordenates of the establishment">
			  </div>
			  <div class="form-group">
			    <label>Schedule</label>
			    <input required type="text" name="schedule" class="form-control" id="ejemplo_email_1"
				   placeholder="Schedule">
			  </div>
			  <div class="form-group">
			    <label>Web page</label>
			    <input required type="text" name="web" class="form-control" id="ejemplo_email_1"
				   placeholder="Web page">
			  </div>
			  <div class="form-group">
			    <label>Email</label>
			    <input required type="text" name="email" class="form-control" id="ejemplo_email_1"
				   placeholder="Email">
			  </div>
			  <div class="form-group">
			    <label>Phone</label>
			    <input required type="text" name="phone" class="form-control" id="ejemplo_email_1"
				   placeholder="Phone">
			  </div>

			  <button type="submit" class="btn btn-primary">Do it!</button>
            		  <a href="../controllers/establishmentController.php?action=dataget" class="btn btn-primary">Go Back</a>
			  </div>

			</form>
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <h3>
    <?php
	if(isset($_GET['val'])){
		$msg = unserialize($_GET['val']);
                echo "<script>";
		echo "alert('$msg')";
		echo "</script>";
	}
	if(isset($_GET['upd'])){
		$msg = unserialize($_GET['upd']);
                echo "<script>";
		echo "alert('$msg')";
		echo "</script>";
	}
	if(isset($_GET['err'])){
		$msg = unserialize($_GET['err']);
                echo "<script>";
		echo "alert('$msg')";
		echo "</script>";
	}
    ?>
    </h3>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>


