<DOCTYPE <!DOCTYPE html>
    <html>
        <head>

            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>SignUp</title>

		<script>
			function checkPass(){
				var pass1 = document.getElementById("pass1").value;
				var pass2 = document.getElementById("pass2").value;

				if(pass1 == pass2){
				alert("|INFO| los password no pueden coincidir");
				}

			}
		</script>

            <!-- Bootstrap Core CSS -->
            <link href="../css/bootstrap.min.css" rel="stylesheet">

            <!-- Custom CSS -->
            <link href="../css/stylish-portfolio.css" rel="stylesheet">
            <!--Custom CSS ABP -->
            <!--link href="../css/signUp.css" rel="stylesheet"-->

            <!-- Custom CSS -->
            <link href="../css/heroic-features.css" rel="stylesheet">
            <!--link href="../css/stylish-portfolio.css" rel="stylesheet-->
            <!--Custom CSS ABP -->
            <link href="../css/signUpPopular.css" rel="stylesheet">

            <!-- Custom Fonts -->
            <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

        </head>
        <body>

            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../index.php">Pinchos</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>


            <form action="../controllers/signUp.php" onsubmit="return comprobar()"  method="POST">
                <div id="container" class="container" style="padding-top:60px; padding-rigth:100; float:left;" >
                    <!--div style="float:right; position:relative;">
                        <img src="../img/enjoying.jpg"style ="padding-left:200;"alt="Imagen" style="width:100px;height:100px;">
                    </div-->
                    <h1 id="SigUpHeader" class="SignUpHeader">Sign Up</h1>
		     <?php
			if(isset($_GET["msg"])){
				$msg = $_GET["msg"];
				echo($msg);
			}
		     ?>
                    <h6> Name </h6>
                    <input  type="text" name="name" class="from-control form-pers" placeholder="Fill this gap with your name" title="Please type a valid name" required pattern="[a-zA-Z0-9\s]+">
                    <h6> Phone </h6>
                    <input type="text" name="phone" class="from-control form-pers" placeholder="Fill this gap with your phone number" required pattern = "^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d">
                    <h6> e-mail </h6>
                    <input required type="email" name="email" class="from-control form-pers" placeholder="Fill this gap with your e-mail">
                    <h6> Password </h6>
                    <input id="pass1" required type="password" name="password" class="from-control form-pers" placeholder="Fill this gap with your password">
                    <h6> Password </h6>
                    <input id="pass2" required type="password" name="password2" class="from-control form-pers" placeholder="Confirm password">
                    <div class="container"style="padding-top:10px">
                        <td colspan=2><cleft><input class = "btn btn-dark btn-lg" type="submit" name="action" value="Ok">
                            <input class = "btn btn-dark btn-lg"type="reset" name="action" value="Cancel" ></left></td>
                            </div>

                            </form>
                            </div>



                            <!-- jQuery -->
                            <script src="../js/jquery.js"></script>

                            <!-- Bootstrap Core JavaScript -->
                            <script src="../js/bootstrap.min.js"></script>

                            <!-- Custom Theme JavaScript -->
                            <script>
                                // Closes the sidebar menu
                                $("#menu-close").click(function (e) {
                                    e.preventDefault();
                                    $("#sidebar-wrapper").toggleClass("active");
                                });

                                // Opens the sidebar menu
                                $("#menu-toggle").click(function (e) {
                                    e.preventDefault();
                                    $("#sidebar-wrapper").toggleClass("active");
                                });

                                // Scrolls to the selected menu item on the page
                                $(function () {
                                    $('a[href*=#]:not([href=#])').click(function () {
                                        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                                            var target = $(this.hash);
                                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                                            if (target.length) {
                                                $('html,body').animate({
                                                    scrollTop: target.offset().top
                                                }, 1000);
                                                return false;
                                            }
                                        }
                                    });
                                });
                            </script>

                            </body>
                            </html>
