<?php
session_start();
if($_SESSION["validated"] != "Popular"){
header("Location: signIn.php");
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Popular Jury Homepage</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/heroic-features.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="homePopularJury.php">Popular Jury Home</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="popularProfile.php">My profile</a>
                        </li>
                        <li>
                            <a href="#">Contest Information</a>
                        </li>
                        <li>
                            <a href="contactUs2.php">Contact Organization</a>
                        </li>
                        <li>
                            <a href="popularVote.php">Vote !</a>
                        </li>
                        <li>
                            <a href="../controllers/popularController.php?action=logout">LogOut</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

	<?php
		if(isset($_GET["pincho1"])){
			$pincho1 = unserialize($_GET["pincho1"]);
			$pincho2 = unserialize($_GET["pincho2"]);
			$pincho3 = unserialize($_GET["pincho3"]);
			$code1 = $_GET["code1"];
			$code2 = $_GET["code2"];
			$code3 = $_GET["code3"];
			$idp1 = $_GET["idp1"];
			$idp2 = $_GET["idp2"];
			$idp3 = $_GET["idp3"];
			$ide1 = $_GET["ide1"];
			$ide2 = $_GET["ide2"];
			$ide3 = $_GET["ide3"];
			$idpop1 = $_GET["idpop1"];
			$idpop2 = $_GET["idpop2"];
			$idpop3 = $_GET["idpop3"];
			$name1 = $pincho1[0]["name"];
			$name2 = $pincho2[0]["name"];
			$name3 = $pincho3[0]["name"];
			$url1 = $pincho1[0]["url"];
			$url2 = $pincho2[0]["url"];
			$url3 = $pincho3[0]["url"];

			//Pincho 1
			echo "<h4>NAME:</h4>".$name1;
			echo "<h4>IMAGE:<h1><img src='".$url1."' width='200' height='200'>";
			echo "<form action='../controllers/popularController.php' method='POST' id='vote'>";
			echo "<input type='hidden' name='idp1' value='$idp1'>";
			echo "<input type='hidden' name='ide1' value='$ide1'>";
			echo "<input type='hidden' name='idpop1' value='$idpop1'>";
			echo "<input type='hidden' name='code11' value='$code1'>";
			echo "<input type='hidden' name='code12' value='$code2'>";
			echo "<input type='hidden' name='code13' value='$code3'>";
			echo "<button type='submit' class='btn btn-primary'>Vote!</button>";
			echo "</form>";

			//Pincho 2
			echo "<h4>NAME:</h4>".$name2;
			echo "<h4>IMAGE:<h1><img src='".$url2."' width='200' height='200'>";
			echo "<form action='../controllers/popularController.php' method='POST' id='vote'>";
			echo "<input type='hidden' name='idp2' value='$idp2'>";
			echo "<input type='hidden' name='ide2' value='$ide2'>";
			echo "<input type='hidden' name='idpop2' value='$idpop2'>";
			echo "<input type='hidden' name='code21' value='$code1'>";
			echo "<input type='hidden' name='code22' value='$code2'>";
			echo "<input type='hidden' name='code23' value='$code3'>";
			echo "<button type='submit' class='btn btn-primary'>Vote!</button>";
			echo "</form>";

			//Pincho 3
			echo "<h4>NAME:</h4>".$name3;
			echo "<h4>IMAGE:<h1><img src='".$url3."' width='200' height='200'>";
			echo "<form action='../controllers/popularController.php' method='POST' id='vote'>";
			echo "<input type='hidden' name='idp3' value='$idp3'>";
			echo "<input type='hidden' name='ide3' value='$ide3'>";
			echo "<input type='hidden' name='idpop3' value='$idpop3'>";
			echo "<input type='hidden' name='code31' value='$code1'>";
			echo "<input type='hidden' name='code32' value='$code2'>";
			echo "<input type='hidden' name='code33' value='$code3'>";
			echo "<button type='submit' class='btn btn-primary'>Vote!</button>";
			echo "</form>";
		}
	?>

        <!-- jQuery -->
        <script src="../js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

    </body>

</html>
