<?php


session_start();
if($_SESSION["validated"] != "Popular"){
header("Location: signIn.php");
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">


        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/heroic-features.css" rel="stylesheet">
        <link href="../css/customGastro.css" rel="stylesheet">

        <title>Gastro map</title>

        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>





         <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="homePopularJury.php">Popular Jury Home</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="popularProfile.php">My profile</a>
                        </li>
                        <li>
                            <a href="popularVote.php">Vote !</a>
                        </li>
                        <li>
                            <a href="../controllers/popularController.php?action=gastromap">Gastro map</a>
                        </li>
                        <li>
                            <a href="../controllers/popularController.php?action=comments">Comments</a>
                        </li>
                        <li>
                            <a href="../controllers/popularController.php?action=logout">LogOut</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
        <header class="jumbotron hero-spacer">
                <h1>Gastromap</h1>
                
            </header>
<div class="gastro">

<?php
if($_POST){
 
    // get latitude, longitude and formatted address
    $data_arr = geocode($_POST['address']);
 
    // if able to geocode the address
    if($data_arr){
         
        $latitude = $data_arr[0];
        $longitude = $data_arr[1];
        $formatted_address = $data_arr[2];
                     
    ?>
 
    <!-- google map will be shown here -->
    <div id="gmap_canvas"></div>
    <div id='map-label'></div>
 
    <!-- JavaScript to show google map -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js"></script>    
    <script type="text/javascript">
        function init_map() {
            var myOptions = {
                zoom: 14,
                center: new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
            marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(<?php echo $latitude; ?>, <?php echo $longitude; ?>)
            });
            infowindow = new google.maps.InfoWindow({
                content: "<?php echo $formatted_address; ?>"
            });
            google.maps.event.addListener(marker, "click", function () {
                infowindow.open(map, marker);
            });
            infowindow.open(map, marker);
        }
        google.maps.event.addDomListener(window, 'load', init_map);
    </script>
 
    <?php
 
    // if unable to geocode the address
    }else{
        echo "No map found.";
    }
}
?>
<?php
if(isset($_GET["datos"])){
$array = unserialize($_GET["datos"]);
echo "<div class='col-lg-12 text-center' style='margin-bottom: 30px; padding-left: 373px'>";
echo "<div class='row'>";
echo "<div class='col-lg-12'>";
echo "</div>";
echo "</div>";
echo "<table class='table table-bordered' style='width:70%';>";
echo "<thead>";
echo "<tr>";
echo "<th>Name</th>";
echo "<th>Coordenates</th>";
echo "</tr>";
echo "</thead>";
echo "<tbody>";

foreach($array as $establecimiento){
	$name = $establecimiento[1];
	$coord = $establecimiento[2];
echo "<tr>";
	echo "<td style = 'width: 50%'>";
	echo $name;
	echo "</td>";
	echo "<td>";
	echo $coord;
	echo "</td>";
echo "<tr>";
}

echo "</tbody>";
echo "</table>";
echo "</div>";
echo "</div>";
}
?>
 
<div id='address-examples'>
   
    <p>Copy and paste coordinates of choosen Establishment from Table.</p>

<!-- enter any address -->
<form action="" method="post" >
    <input type='text' style='margin-left: 625px; margin-top: 10px' name='address' placeholder='Coordenates here' />
    <input type='submit' value='Show ubication' />
</form>
</div>
 
<?php
 
// function to geocode address, it will return false if unable to geocode address
function geocode($address){
 
    // url encode the address
    $address = urlencode($address);
     
    // google map geocode api url
    $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";
 
    // get the json response
    $resp_json = file_get_contents($url);
     
    // decode the json
    $resp = json_decode($resp_json, true);
 
    // response status will be 'OK', if able to geocode given address 
    if($resp['status']=='OK'){
 
        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];
         
        // verify if data is complete
        if($lati && $longi && $formatted_address){
         
            // put the data in the array
            $data_arr = array();            
             
            array_push(
                $data_arr, 
                    $lati, 
                    $longi, 
                    $formatted_address
                );
             
            return $data_arr;
             
        }else{
            return false;
        }
         
    }else{
        return false;
    }
}
?>
 
</body>
</html>
