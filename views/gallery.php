<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Galery</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/stylish-portfolio.css" rel="stylesheet">

        <!-- Custom CSS ABP-->
        <!--link href="../css/gallery.css" rel="stylesheet"-->

        <!-- Custom CSS -->
        <link href="../css/heroic-features.css" rel="stylesheet">
        <!--link href="../css/stylish-portfolio.css" rel="stylesheet-->
        <!--Custom CSS ABP -->
        <!--link href="../css/signIn.css" rel="stylesheet"-->

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    </head>
    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="../index.php">Pinchos</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

	<?php
		if(isset($_GET["msg"])){
			$msg = $_GET["msg"];
			echo($msg);
		}
		if(isset($_GET["array"])){
			$array = unserialize($_GET["array"]);
			echo "<section id='portfolio' class='portfolio'>";
			echo "<div class='container'>";
			echo "<div class='row'>";
			echo "<div class='col-lg-10 col-lg-offset-1 text-center'>";
			echo "<h2>The best Pinchos challenge ever</h2>";
			echo "<hr class='small'>";
			echo "<div class='row'>";
			foreach($array as $pincho){
				$id = $pincho["code"];
				$name = $pincho["name"];
				$description = $pincho["description"];
				$price =  $pincho["price"];
				$ingredients = $pincho["ingredients"];
				$url = $pincho["url"];
				echo "<div class='portfolio-item'>";
				echo "<a href='../controllers/galleryController.php?id=$id&name=$name&description=$description&price=$price&ingredients=$ingredients&url=$url'>";
				echo "<img class='img-portfolio img-responsive' src='".$url."' width='300' height='300'>";
				echo "</a>";
				echo "</div>";
			}
			echo "</div>";
			echo "</div>";
			echo "</div>";
			echo "</div>";
			echo "</section>";
		}
	?>
        
            

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1 text-center">
                        <h4><strong>ABP</strong>
                        </h4>
                        <p>ESEI<br>Orense</p>
                        <ul class="list-unstyled">
                            <li><i class="fa fa-phone fa-fw"></i> 669725696</li>
                            <li><i class="fa fa-envelope-o fa-fw"></i>  <a href="mailto:name@example.com">adotero@esei.uvigo.es</a>
                            </li>
                        </ul>
                        <br>
                        <ul class="list-inline">
                            <li>
                                <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
                            </li>
                        </ul>
                        <hr class="small">
                        <p class="text-muted">Copyright &copy; Pinchos 2015</p>
                    </div>
                </div>
            </div>
        </footer>

        <!-- jQuery -->
        <script src="../js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script>
            // Closes the sidebar menu
            $("#menu-close").click(function (e) {
                e.preventDefault();
                $("#sidebar-wrapper").toggleClass("active");
            });

            // Opens the sidebar menu
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#sidebar-wrapper").toggleClass("active");
            });

            // Scrolls to the selected menu item on the page
            $(function () {
                $('a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });
        </script>

    </body>

</html>
