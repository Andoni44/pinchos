<?php
session_start();
if($_SESSION["validated"] != "Popular"){
header("Location: signIn.php");
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Comments page</title>

        <!-- Bootstrap Core CSS -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../css/heroic-features.css" rel="stylesheet">
        <link href="../css/customPopular.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
 <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="homePopularJury.php">Popular Jury Home</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="popularProfile.php">My profile</a>
                        </li>
                        <li>
                            <a href="popularVote.php">Vote !</a>
                        </li>
                        <li>
                            <a href="../controllers/popularController.php?action=gastromap">Gastro map</a>
                        </li>
                        <li>
                            <a href="../controllers/popularController.php?action=comments">Comments</a>
                        </li>
                        <li>
                            <a href="../controllers/popularController.php?action=logout">LogOut</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

	<?php
		if(isset($_GET["msg"])){
			$msg = $_GET["msg"];
			echo($msg);
		}
		if(isset($_GET["array"])){
			$array = unserialize($_GET["array"]);
			echo "<section id='portfolio' class='portfolio'>";
			echo "<div class='container'>";
			echo "<div class='row'>";
			echo "<div class='col-lg-10 col-lg-offset-1 text-center'>";
			echo "<h2>Select the pincho for add a comment.</h2>";
			echo "<hr class='small'>";
			echo "<div class='row'>";
			foreach($array as $pincho){
				$id = $pincho["code"];
				$name = $pincho["name"];
				$description = $pincho["description"];
				$price =  $pincho["price"];
				$ingredients = $pincho["ingredients"];
				$url = $pincho["url"];
				echo "<div class='portfolio-item'>";
				echo "<a href='../controllers/popularController.php?idpincho=$id'>";
				echo "<img class='img-portfolio img-responsive' src='".$url."' width='300' height='300'>";
				echo "</a>";
				echo "</div>";
			}
			echo "</div>";
			echo "</div>";
			echo "</div>";
			echo "</div>";
			echo "</section>";
		}
	?>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Pinchos Contest 2015</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../js/bootstrap.min.js"></script>

    </body>

</html>
