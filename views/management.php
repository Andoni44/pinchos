<?php
session_start();
if($_SESSION["validated"] != "Organizer"){
  header("Location: signIn.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Management</title>

  <!-- Bootstrap Core CSS -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="../css/heroic-features.css" rel="stylesheet">
  <link href="../css/customManagement.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="homeOrganizer.php">Organizer</a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li>
            <a href="createProfessional.php">Create Professional</a>
          </li>
          <li>
            <a href="../controllers/organizerController.php?action=evalidation">Establishments Validation</a>
          </li>
          <li>
            <a href="../controllers/organizerController.php?action=pvalidation">Pinchos validation</a>
          </li>
          <li>
            <a href="../controllers/organizerController.php?action=assign">Professional Assignements</a>
          </li>
          <li>
            <a href="../controllers/organizerController.php?action=management">Management</a>
          </li>
	  <li>
            <a href="../controllers/organizerController.php?action=winner">Winner Selection</a>
	  </li>
          <li>
            <a href="../controllers/organizerController.php?action=logout">LogOut</a>
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </nav>

  <div class="row" style="padding-top: 40px; padding-left: 170px">
    <div class="col-md-3"><h3>Establishments</h3>
	<?php
		if(isset($_GET["establishments"])){
			$establishments = unserialize($_GET["establishments"]);
			if($establishments == false){

			}else{
				echo "<form action='../controllers/organizerController.php' method='POST' id='estDelete'>";
				foreach($establishments as $establishment){
				$nameE = $establishment["name"];
				$idE = $establishment["idEstablishment"];
				echo "<div class='checkbox'>";
				echo "<label><input type='radio' name='idE' value=$idE>$nameE</label>";
				echo "</div>";
				}
     				echo "<div class='container' style='padding-top:10px; align-content:left;'>";
         			echo "<td colspan=2><cleft><input class = 'btn' type='submit' value='Delete' style='font-size: 100%;'>";
      				echo "</div>";
				echo "</form>";
			}
		}
	?>
     </div>

    <div class="col-md-3"><h3>Pinchos</h3>
	<?php
		if(isset($_GET["pinchos"])){
			$pinchos = unserialize($_GET["pinchos"]);
			if($pinchos == false){

			}else{
				echo "<form action='../controllers/organizerController.php' method='POST' id='estDelete'>";
				foreach($pinchos as $pincho){
				$nameP = $pincho["name"];
				$idP = $pincho["code"];
				echo "<div class='checkbox'>";
				echo "<label><input type='radio' name='idP' value=$idP>$nameP</label>";
				echo "</div>";
				}
     				echo "<div class='container' style='padding-top:10px; align-content:left;'>";
         			echo "<td colspan=2><cleft><input class = 'btn' type='submit' value='Delete' style='font-size: 100%;'>";
      				echo "</div>";
				echo "</form>";

			}
		}
	?>



    </div>

    <div class="col-md-3"><h3>Popular</h3>
	<?php
		if(isset($_GET["popular"])){
			$popular = unserialize($_GET["popular"]);
			if($popular == false){

			}else{
				echo "<form action='../controllers/organizerController.php' method='POST' id='estDelete'>";
				foreach($popular as $popu){
				$namePo = $popu["name"];
				$idPo = $popu["idPopular"];
				echo "<div class='checkbox'>";
				echo "<label><input type='radio' name='idPo' value=$idPo>$namePo</label>";
				echo "</div>";
				}
     				echo "<div class='container' style='padding-top:10px; align-content:left;'>";
         			echo "<td colspan=2><cleft><input class = 'btn' type='submit' value='Delete' style='font-size: 100%;'>";
      				echo "</div>";
				echo "</form>";

			}
		}
	?>



    </div>


    <div class="col-md-3"><h3>Professional</h3>
	<?php
		if(isset($_GET["professional"])){
			$professional = unserialize($_GET["professional"]);
			if($professional == false){

			}else{
				echo "<form action='../controllers/organizerController.php' method='POST' id='estDelete'>";
				foreach($professional as $pro){
				$namePro = $pro["name"];
				$idPro = $pro["idProfessional"];
				echo "<div class='checkbox'>";
				echo "<label><input type='radio' name='idPro' value=$idPro>$namePro</label>";
				echo "</div>";
				}
     				echo "<div class='container' style='padding-top:10px; align-content:left;'>";
         			echo "<td colspan=2><cleft><input class = 'btn' type='submit' value='Delete' style='font-size: 100%;'>";
      				echo "</div>";
				echo "</form>";

			}
		}
	?>

    </div>
  </div>

  <!-- jQuery -->
  <script src="js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="js/bootstrap.min.js"></script>

</body>

</html>
