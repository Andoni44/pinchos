<DOCTYPE <!DOCTYPE html>
    <html>
        <head>

            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>SignIn</title>

            <!-- Bootstrap Core CSS -->
            <link href="../css/bootstrap.min.css" rel="stylesheet">

            <!-- Custom CSS -->
            <link href="../css/heroic-features.css" rel="stylesheet">
            <!--link href="../css/stylish-portfolio.css" rel="stylesheet-->
            <!--Custom CSS ABP -->
            <link href="../css/signIn.css" rel="stylesheet">

            <!-- Custom Fonts -->
            <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

        </head>
        <body>

            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../index.php">Pinchos</a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>



            <form action="../controllers/user_validation.php" method="POST" id="login">
                <div id="container" class="container" style="padding-top:60px; padding-rigth:100; float:left;" >
                    <!--div style="float:right; position:relative;">
                        <img src="../img/woman-cooking.jpg"style ="padding-left:200;"alt="Imagen" style="width:100px;height:100px;">
                    </div-->
                    <h1 id="SigUpHeader" class="SignUpHeader">Sign In</h1>
                    <h6> Id </h6>
                    <input type="text" name="id" class="from-control form-pers" placeholder="Fill this gap with id">
                    <h6> Password </h6>
                    <input type="password" name="password" class="from-control form-pers" placeholder="Fill this gap with your password">
                    <h6> Select kind of user </h6>
                    <select name="userList" form="login">
                        <option value="pro">Professional</option>
                        <option value="popular">Popular</option>
                        <option value="establishment">Establishment</option>
                        <option value="organizer">Organizer</option>
                    </select>
                    <div class="container"style="padding-top:10px">
                        <td colspan=2><cleft><input class = "btn btn-dark btn-lg" type="submit" name="action" value="Ok">
                            <input class = "btn btn-dark btn-lg" type="reset" name="action" value="Cancel"></left></td>
                            </div>

                            </form>
                            <?php
                            if (isset($_GET["msg"])) {
                                $msg = $_GET["msg"];
                                echo "<script>";
				echo "alert('$msg')";
				echo "</script>";
                            }
                            ?>
                            </div>





                            <!-- jQuery -->
                            <script src="../js/jquery.js"></script>

                            <!-- Bootstrap Core JavaScript -->
                            <script src="../js/bootstrap.min.js"></script>

                            <!-- Custom Theme JavaScript -->
                            <script>
                                // Closes the sidebar menu
                                $("#menu-close").click(function (e) {
                                    e.preventDefault();
                                    $("#sidebar-wrapper").toggleClass("active");
                                });

                                // Opens the sidebar menu
                                $("#menu-toggle").click(function (e) {
                                    e.preventDefault();
                                    $("#sidebar-wrapper").toggleClass("active");
                                });

                                // Scrolls to the selected menu item on the page
                                $(function () {
                                    $('a[href*=#]:not([href=#])').click(function () {
                                        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                                            var target = $(this.hash);
                                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                                            if (target.length) {
                                                $('html,body').animate({
                                                    scrollTop: target.offset().top
                                                }, 1000);
                                                return false;
                                            }
                                        }
                                    });
                                });
                            </script>

                            </body>
                            </html>
