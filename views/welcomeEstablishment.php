<?php
session_start();
if($_SESSION["validated"] != "Establishment"){
header("Location: signIn.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Heroic Features - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/heroic-features.css" rel="stylesheet">
    <link href="../css/customEstablishment.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../controllers/establishmentController.php?action=dataget">Establishment</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="editProfileEstablishment.php">Edit Profile</a>
                    </li>
                    <li>
                        <a href="requestManagement.php">Request Management</a>
                    </li>
                    <li>
                        <a href="../controllers/pinchoController.php?action=view">View Pincho</a>
                    </li>
		    <li>
			<a href="../controllers/establishmentController.php?action=code">Code Generation</a>
		    </li>
		    <li>
			<a href="../controllers/establishmentController.php?action=logout">LogOut</a>
		    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Jumbotron Header -->
        <header class="jumbotron hero-spacer">
           
            <h1>
		<?php
		echo($_SESSION["name"]);
		?>
	    </h1>
        </header>

        <hr>

        <!-- /.row -->
<div class="col-lg-12 text-center" style="margin-bottom: 30px;">
			<div class="row">
			    <div class="col-lg-12">
				<h3>Personal info</h3>
			    </div>
			</div>
                    <table class="table table-bordered">
			<thead>
			      	<tr>
				  <th>Name</th>
				  <th>Coordenates</th>
				  <th>Schedule</th>
				  <th>Web Page</th>
				  <th>Email</th>
				  <th>Phone</th>
			        </tr>
   			</thead>
			<tbody>
			        <tr>
				  <td>
				  <?php
				  echo $_GET["name"];
				  ?>
				  </td>
				  <td>
			          <?php
				  echo $_GET["coordenates"];
				  ?>
				  </td>
				  <td>
			          <?php
				  echo $_GET["schedule"];
				  ?>
				  </td>
				  <td>
			          <?php
				  echo $_GET["webpage"];
				  ?>
				  </td>
				  <td>
			          <?php
				  echo $_GET["email"];
				  ?>
				  </td>
				  <td>
			          <?php
				  echo $_GET["phone"];
				  ?>
				  </td>
				</tr>
			</tbody>
		    </table>
			<a href="editProfileEstablishment.php" class="btn btn-primary">Edit Profile</a>
			<a href="requestManagement.php" class="btn btn-primary">Request Management</a>
			<a href="viewPinchoEstablishment.php" class="btn btn-primary">View Pincho</a>
                </div>

        <hr>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
