<?php

require_once("database.php");

class Pincho {

    private $db;
    private $con;

    public function __construct() {
        $this->db = new Database();
        $this->con = $this->db->getConnection();
    }

    //Funcion para añadir un nuevo pincho. Se recomienda utilizar previamente la funcion isValid
    public function insert($pincho) {
        $name = $pincho["name"];
        $description = $pincho["description"];
        $price = $pincho["price"];
        $ingredients = $pincho["ingredients"];
        $url = $pincho["url"];
        $idEstablishment = $pincho["idEstablishment"];

        $sql = 'insert into Pincho (name,description,price,ingredients,url,Establishment_idEstablishment) values ("' . $name . '","' . $description . '","' . $price . '","' . $ingredients . '","' . $url . '",' . $idEstablishment . ')';
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Función que devuelve el pincho por su code
    public function getbyCode($code){
	$sql = 'select * from Pincho where code=' . $code;
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }	
    }

    //Funcion para marcar a un pincho como finalista
    public function setFinalist($id){
   	$sql = 'update Pincho set isFinalist=1 where code='.$id;
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Funcion que devuelve datos del pincho a partir del nombre
    public function getData($name){
	$sql = 'select * from Pincho where name="' . $name.'"';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Funcion que devuelve el pincho de un establecimiento a partir de su ID
    public function select($id) {
        $sql = 'select * from Pincho where Establishment_idEstablishment=' . $id;
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Funcion que selecciona todos los pinchos no validados
    public function selectNotValidated(){
	$sql = 'select * from Pincho where isValidated is NULL';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Funcion que devuelve todos los pinchos validados en el sistema.
    public function selectAll() {
        $sql = 'select * from Pincho where isValidated=1';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Funcion que devuelve los pinchos finalistas elegidos por los miembros del jurado profesional
    public function selectFinalists(){
	$sql = 'select * from Pincho where isFinalist=1';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Función que selecciona al pincho ganador
    public function selectWinner(){
	$sql = 'select * from Pincho where isWinner=1';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Funcion que pone el campo isWinner de todos los pinchos a 0
    public function doAllLoser(){
	$sql = 'update Pincho set isWinner=0';
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Función que elige 1 ganador
    public function doWinner($id){
	$sql = 'update Pincho set isWinner=1 where code='.$id;
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Select All pinchos name
    public function selectnAll(){
        $sql = 'select name,code from Pincho';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }	
    }

    //Funcion que valida el pincho a partir del nombre
    public function validatePincho($name){
	$sql = 'update Pincho set isValidated=1 where name="'.$name.'"';
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Funcion que borra el pincho asociado al establecimiento
    public function borradoEst($idE){
	    $sql = 'delete from Pincho where Establishment_idEstablishment="'.$idE.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		echo("Error borrando de professionalValorations");
		return false;
	    }
    }

    //Funcion que borra datos relativos al pincho que se quiere borrar
    public function borradoPin($idP){
	    $sql = 'delete from Pincho where code="'.$idP.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		echo("Error borrando de professionalValorations");
		return false;
	    }
    }


    //Funcion que comprueba si los datos del pincho son correctos para introducir en el sistema
    public function isValid($pincho) {
        $name = $pincho["name"];
        $description = $pincho["description"];
        $price = $pincho["price"];
        $ingredients = $pincho["ingredients"];
        $url = $pincho["url"];
        $idEstablishment = $pincho["idEstablishment"];

        if ($name == "") {
            return false;
        } else if ($description == "") {
            return false;
        } else if ($price == "") {
            return false;
        } else if ($ingredients == "") {
            return false;
        } else if ($url == "") {
            return false;
        } else {
            $sql = 'select * from Pincho where Establishment_idEstablishment=' . $idEstablishment;
            $result = mysql_query($sql, $this->con);
            if (mysql_num_rows($result) == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

}

?>
