<?php

require_once("database.php");

class Establishment {

    private $db;
    private $con;

    public function __construct() {
        $this->db = new Database();
        $this->con = $this->db->getConnection();
    }

    public function insertEstablishment($establishment) {
        $name = $establishment["name"];
        $coordenates = $establishment["coordenates"];
        $phone = $establishment["phone"];
        $email = $establishment["email"];
        $schedule = $establishment["schedule"];
        $webpage = $establishment["webpage"];
        $pass = $establishment["password"];

        $sql = 'insert into Establishment(name,coordenates,phone,email,schedule,webpage,password) values ("' . $name . '","' . $coordenates . '","' . $phone . '","' . $email . '","' . $schedule . '","' . $webpage . '","' . $pass . '")';
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo("\n");
            return false;
        }
    }

    //Select All establishments name and id
    public function selectAll(){
        $sql = 'select name,idEstablishment from Establishment';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }	
    }

    //Select establishment data by name
    public function select($id) {
        $sql = 'select * from Establishment where name="' . $id.'"';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

     public function selectData() {
        $sql = 'select * from Establishment';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Select all not validated establishments
    public function selectNotValidated(){
	$sql = 'select * from Establishment where isValidated is NULL';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Update establishment data function
    public function update($establishment, $id) {
        $name = $establishment["name"];
        $coordenates = $establishment["coordenates"];
        $schedule = $establishment["schedule"];
        $web = $establishment["web"];
        $email = $establishment["email"];
        $phone = $establishment["phone"];

        $sql = 'update Establishment set name="' . $name . '",coordenates="' . $coordenates . '",schedule="' . $schedule . '",webpage="' . $web . '",email="' . $email . '",phone="' . $phone . '" where name="' . $id.'"';

        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Funcion que valida el establecimiento a partir del nombre
    public function validateEstablishment($name){
	$sql = 'update Establishment set isValidated=1 where name="'.$name.'"';
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    public function exists($establishment){
        $name = $establishment["name"];
	$sql = 'select * from Establishment where name="' . $name . '"';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return true;
        } else {
            return false;
        }	
    }

    //Funcion que borra el establecimiento
    public function borradoEst($idE){
	    $sql = 'delete from Establishment where idEstablishment="'.$idE.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		echo("Error borrando de professionalValorations");
		return false;
	    }
    }

    //Validation function for update
    public function isValid($establishment) {
        $name = $establishment["name"];
        $coordenates = $establishment["coordenates"];
        $schedule = $establishment["schedule"];
        $web = $establishment["web"];
        $email = $establishment["email"];
        $phone = $establishment["phone"];

        if ($name == "") {
            return false;
        } else if ($coordenates == "") {
            return false;
        } else if ($schedule == "") {
            return false;
        } else if ($web == "") {
            return false;
        } else if ($email == "") {
            return false;
        } else if ($phone == "") {
            return false;
        } else {
            return true;
        }
    }

}

?>
