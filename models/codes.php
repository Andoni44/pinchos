<?php
require_once("database.php");
class Codes{
    private $db;
    private $con;

    public function __construct() {
        $this->db = new Database();
        $this->con = $this->db->getConnection();
    }

    public function insert($array){
	$idEstablishment = $array["Pincho_Establishment_idEstablishment"];
	$idPincho = $array["Pincho_code"];
	$idPopular = $array["Popular_idPopular"];
	$codigoAleatorio = $array["Code"];
        $sql = 'insert into Codes (Code,Popular_idPopular,Pincho_code,Pincho_Establishment_idEstablishment) values ("' . $codigoAleatorio . '","' . $idPopular . '","' . $idPincho . '","' . $idEstablishment .'")';
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Funcion que comprueba que el código existe y devuelve la información en ese caso
    public function check($codigo){
	    $sql = 'select * from Codes where Code="' . $codigo.'" and wasUsed is NULL';
            $result = mysql_query($sql, $this->con);
            if (mysql_num_rows($result) == 0) {
                return false;
            } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;             
            }
    }

    public function delete($code){
	    $sql = 'delete from Codes where Code="'.$code.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		return false;
	    }
    }

    //Funcion para marcar un codigo como usado
    public function setUsed($code){
	    $sql = 'update Codes set wasUsed="1" where Code="'.$code.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		return false;
	    }
    }

    //Funcion que comprueba que el codigo introducido no exista
    public function isValid($codigo){
            $sql = 'select * from Codes where Code="' . $codigo.'"';
            $result = mysql_query($sql, $this->con);
            if (mysql_num_rows($result) == 0) {
                return true;
            } else {
                return false;
            }
    }
}
?>
