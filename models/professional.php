<?php

require_once("database.php");

class Professional {

    private $db;
    private $con;

    public function __construct() {
        $this->db = new Database();
        $this->con = $this->db->getConnection();
    }

    //Funcion para insertar un profesional en la base de datos
    public function addPro($professional) {
        $name = $professional["name"];
        $email = $professional["email"];
        $phone = $professional["phone"];
        $password = $professional["password"];
        $sql = 'insert into Professional (name,email,phone,password) values ("' . $name . '","' . $email . '","' . $phone . '","' . $password . '")';
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Funcion que borra datos relativos al profesional que se quiere borrar
    public function borradoProfessional($idPro){
	    $sql = 'delete from Professional where idProfessional="'.$idPro.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		echo("Error borrando de professionalValorations");
		return false;
	    }
    }

    //Select All professional name and id
    public function selectAll(){
        $sql = 'select name,idProfessional from Professional';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }	
    }

    //Funcion para recoger la id de un professional 
    public function getId($name){
	$sql = 'select idProfessional from Professional where name="' . $name.'"';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return false;
        } else {
            $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
        }
    }

    //Funcion que comprueba que el profesional que se trata de insertar no esta en la base de datos
    public function isValid($professional) {
        $name = $professional["name"];
        $sql = 'select * from Professional where name="' . $name . '"';
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return true;
        } else {
            return false;
        }
    }

    //Update professional data function
    public function update($professional, $id) {
        $name = $professional["name"];
	$pass = $professional["pass"];
	$email = $professional["email"];
	$phone = $professional["phone"];

        $sql = 'update Professional set name="' . $name . '",password="' . $pass . '",email="' . $email . '",phone="' . $phone . '" where name="' . $id.'"';

        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

}

?>
