<?php
require_once("database.php");
class ProfessionalValorations{

    private $db;
    private $con;

    public function __construct() {
        $this->db = new Database();
        $this->con = $this->db->getConnection();
    }

    //Funcion de inserción que completa la asignación del pincho al professional
    public function insert($idPincho,$idProfessional,$idEstablishment){
	$sql = 'insert into ProfessionalValorations (Pincho_code,Professional_idProfessional,Pincho_Establishment_idEstablishment) values (' . $idPincho . ',' . $idProfessional . ',' . $idEstablishment . ')';
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Funcion que devuelve las ids de los pinchos asignados a un profesional
    public function getAssigned($idPro){
	$sql = 'select Pincho_code from ProfessionalValorations where Professional_idProfessional='.$idPro;
	$result = mysql_query($sql,$this->con);
	if(mysql_num_rows($result) == 0){
		return false;
	}else{
	    $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
	}
	
    }

    //Funcion que borra datos relativos al establecimiento que se quiere borrar
    public function borradoEst($idE){
	    $sql = 'delete from ProfessionalValorations where Pincho_Establishment_idEstablishment="'.$idE.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		echo("Error borrando de professionalValorations");
		return false;
	    }
    }

    //Funcion que borra datos relativos al pincho que se quiere borrar
    public function borradoPin($idP){
	    $sql = 'delete from ProfessionalValorations where Pincho_code="'.$idP.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		echo("Error borrando de professionalValorations");
		return false;
	    }
    }

    //Funcion que borra datos relativos al profesional que se quiere borrar
    public function borradoProfessional($idPro){
	    $sql = 'delete from ProfessionalValorations where Professional_idProfessional="'.$idPro.'"';
	    $result = mysql_query($sql,$this->con);
	    if($result == true){
		return true;
	    }else{
		echo("Error borrando de professionalValorations");
		return false;
	    }
    }

    public function doVotation($nota,$idPincho){
	$sql = 'update ProfessionalValorations set Calification='.$nota.' where Pincho_code='.$idPincho;
        $result = mysql_query($sql, $this->con);
        if ($result == true) {
            return true;
        } else {
            echo(mysql_error());
            return false;
        }
    }

    //Funcion que devuelve la nota de un pincho
    public function getVote($id){
	$sql = 'select Calification from ProfessionalValorations where Pincho_code='.$id;
	$result = mysql_query($sql, $this->con);
	if(mysql_num_rows($result) == 0){
		return false;
	}else{
	    $toret = array();
            while ($row = mysql_fetch_assoc($result)) {
                $toret[] = $row;
            }
            return $toret;
	}
		
    }

    //Funcion que comprueba que un pincho no ha sido asignado ya a un profesional
    public function isValid($idPincho){
	$sql = 'select * from ProfessionalValorations where Pincho_code='.$idPincho;
        $result = mysql_query($sql, $this->con);
        if (mysql_num_rows($result) == 0) {
            return true;
        } else {
	    return false;
        }
    }
}
?>
