<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ABP PROYECT</title>

  <!-- Bootstrap Core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="css/stylish-portfolio.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>

  <!-- Navigation -->
  <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i> Menu</a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
      <li class="sidebar-brand">
        <a href="#top"  onclick = $("#menu-close").click(); >Menu</a>
      </li>
      <li>
        <a href="#top" onclick = $("#menu-close").click(); >Home</a>
      </li>
      <li>
        <a href="views/signIn.php" onclick = $("#menu-close").click(); >Sign in</a>
      </li>
      <li>
        <a href="views/signUpPopular.php" onclick = $("#menu-close").click(); >Sign up as a Person</a>
      </li>
      <li>
        <a href="views/signUpEstablishment.php" onclick = $("#menu-close").click(); >Sign up as a establishment</a>
      </li>
      <li>
        <a href="controllers/galleryController.php?action=finalists" onclick = $("#menu-close").click(); >View Finalists</a>
      </li>
      <li>
        <a href="controllers/galleryController.php?action=winner" onclick = $("#menu-close").click(); >View Winner</a>
      </li>
      <li>
        <a href="views/contactUs.php" onclick = $("#menu-close").click(); >Contact</a>
      </li>
    </ul>
  </nav>

  <!-- Header -->
  <header id="top" class="header">
    <div class="text-vertical-center">
      <h1 style="color: #d9d9d9">Pinchos</h1>
      <h3 style="color: #d9d9d9">The best way to contests of pinchos</h3>
      <br>
      <a href="controllers/galleryController.php" class="btn btn-dark btn-lg">Gallery</a>
    </div>
  </header>
  <!-- Map -->
  <section id="contact" class="map">
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div style="overflow:hidden;height:100%;width:100%;"><div id="gmap_canvas" style="height:100%;width:100%;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.map-embed.com" id="get-map-data">www.map-embed.com</a></div><script type="text/javascript"> function init_map() {
      var myOptions = {zoom: 16, center: new google.maps.LatLng(42.3434743, -7.856379400000037), mapTypeId: google.maps.MapTypeId.ROADMAP};
      map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
      marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(42.3434743, -7.856379400000037)});
      infowindow = new google.maps.InfoWindow({content: "<b>ourense</b><br/>5 Rua Nova<br/>32004 Ourense"});
      google.maps.event.addListener(marker, "click", function () {
        infowindow.open(map, marker);
      });
      infowindow.open(map, marker);
    }
    google.maps.event.addDomListener(window, 'load', init_map);</script>
  </section>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-lg-offset-1 text-center">
          <h4><strong>ABP</strong>
          </h4>
          <p>ESEI<br>Orense</p>
          <ul class="list-unstyled">
            <li><i class="fa fa-phone fa-fw"></i> 669725696</li>
            <li><i class="fa fa-envelope-o fa-fw"></i>  <a href="mailto:name@example.com">adotero@esei.uvigo.es</a>
            </li>
          </ul>
          <br>
          <ul class="list-inline">
            <li>
              <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
            </li>
            <li>
              <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
            </li>
            <li>
              <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
            </li>
          </ul>
          <hr class="small">
          <p class="text-muted">Copyright &copy; Pinchos 2015</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- jQuery -->
  <script src="js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="js/bootstrap.min.js"></script>

  <!-- Custom Theme JavaScript -->
  <script>
  // Closes the sidebar menu
  $("#menu-close").click(function (e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });

  // Opens the sidebar menu
  $("#menu-toggle").click(function (e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });

  // Scrolls to the selected menu item on the page
  $(function () {
    $('a[href*=#]:not([href=#])').click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });
  </script>

</body>

</html>
