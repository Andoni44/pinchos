<?php
session_start();
require_once("../models/establishment.php");
require_once("../models/pincho.php");
require_once("../models/popular.php");
require_once("../models/codes.php");
/*Check for data comes from Establishment edit profile form.*/
if(isset($_POST["name"]) && isset($_POST["coordenates"]) && isset($_POST["schedule"]) && isset($_POST["web"]) && isset($_POST["email"]) && isset($_POST["phone"])){
	$establishment = array();
	$establishment["name"] = $_POST["name"];	
	$establishment["coordenates"] = $_POST["coordenates"];
	$establishment["schedule"] = $_POST["schedule"];
	$establishment["web"] = $_POST["web"];
	$establishment["email"] = $_POST["email"];
	$establishment["phone"] = $_POST["phone"];
	update($establishment);
}else if(isset($_POST["namePopular"])){
	$namePopular = $_POST["namePopular"];
	codeGeneration($namePopular);
}else{
	$action = $_GET["action"];
	if($action == "dataget"){
		getData();
	}else if($action == "code"){
		viewPincho();
	}else if($action == "logout"){
		session_start();
		$_SESSION["validated"]="";
		session_destroy();
		header("Location: ../index.php");
	}else{
	//Procesamiento de futuras peticiones que vengan por GET
	echo("No recibe los datos del GET");
	}
}

//Update establishment data control
function update($establishment){
	$e = new Establishment();
	$boolean = $e->isValid($establishment);
	if($boolean == false){
		$msg = serialize("Validation fail. Fill all form inputs.");
		header("Location: ../views/editProfileEstablishment.php?val=$msg");
	}else{
		$id = $_SESSION["name"];
		$bool = $e->update($establishment,$id);
		if($bool == true){
			$_SESSION["name"] = $establishment["name"];
			$msg = serialize("Your profile have been updated.");
			header("Location: ../views/editProfileEstablishment.php?upd=$msg");
		}else{
			$msg = serialize("Server Error: Profile not changed");
			header("Location: ../views/editProfileEstablishment.php?err=$msg");
		}
	}
}

//Get establishment data for welcomeEstablishment page
function getData(){
	$aux = new Establishment();
	$name = $_SESSION["name"];
	$bool = $aux->select($name);
	if($bool == false){
		echo("Error del servidor");
	}else{
		$name = $bool[0]["name"];
		$coordenates = $bool[0]["coordenates"];
		$phone = $bool[0]["phone"];
		$email = $bool[0]["email"];
		$schedule = $bool[0]["schedule"];
		$webpage = $bool[0]["webpage"];
		header("Location: ../views/welcomeEstablishment.php?name=$name&coordenates=$coordenates&phone=$phone&email=$email&schedule=$schedule&webpage=$webpage");
	}
}

function viewPincho(){
	$aux = new Establishment();
	$name = $_SESSION["name"];
	$bool = $aux->select($name);
	if($bool == false){
		echo("Error del servidor");
	}else{
		$id = $bool[0]["idEstablishment"];
		$p = new Pincho();
		$boolean = $p->select($id);
		if($boolean == false){
			header("Location: ../views/notPinchoEstablishment.php");
		}else{
			header("Location: ../views/generateCode.php");
		}
	}
}

function codeGeneration($namePopular){
//Buscar el id del popular conociendo el nombre
//Buscar el id del establecimiento actual
//Buscar el id del pincho del establecimiento actual
//Crear un array que contenga la id del establecimiento, la id del popular, la id del pincho y el codigo aleatorio generado, para insertarlo en la tabla codes.
	$e = new Establishment();
	$name = $_SESSION["name"];
	$bool = $e->select($name);
	if($bool == false){
		echo("Error del servidor");
	}else{
		$idEstablishment = $bool[0]["idEstablishment"];
		$p = new Pincho();
		$boolean = $p->select($idEstablishment);
		if($boolean == false){
			echo("Error del servidor");
		}else{
			$idPincho = $boolean[0]["code"];
			$popular = new Popular();
			$comp = $popular->select($namePopular);
			if($comp == false){
				$msg = "El popular introducido no existe";
				header("Location: ../views/generateCode.php?msg=$msg");
			}else{
				$idPopular = $comp[0]["idPopular"];
				$codigoAleatorio = generarCodigo(10) ;
				$cod = new Codes();
				$tValid = $cod->isValid($codigoAleatorio);
				if($tValid == false){
					$msg = "Error interno al generar el código.Repita el proceso";
					header("Location: ../views/generateCode.php?msg=$msg");
				}else{
					$array = array();
					$array["Pincho_Establishment_idEstablishment"] = $idEstablishment;
					$array["Pincho_code"] = $idPincho;
					$array["Popular_idPopular"] = $idPopular;
					$array["Code"] = $codigoAleatorio;
					$inserta = $cod->insert($array);
					if($inserta == true){
						$msg = "El codigo generado es $codigoAleatorio";
						header("Location: ../views/generateCode.php?msg=$msg");
					}else{
						echo("Error en la inserción");
					}
				}
			}
		}
	}
}

function generarCodigo($longitud) {
 $key = '';
 $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;
}
?>
