<?php
require_once("../models/user.php");
if(isset($_POST["id"]) && isset($_POST["password"]) && isset($_POST["userList"])){
	$userType = $_POST["userList"];
	$user = array();
	$user["name"] = $_POST["id"];
	$user["password"] = $_POST["password"];
	if($userType == "popular"){
		popular($user);
	}else if($userType == "pro"){
		professional($user);
	}else if($userType == "establishment"){
		establishment($user);
	}else{
		organizer($user);
	}
}else{
echo("No llegan los datos del formulario.");
}

function popular($user){
	$u = new User();
	$boolean = $u->selectPopular($user);
	if($boolean == true){
		session_start();
		$_SESSION["validated"] = "Popular";
		$_SESSION["name"] = $user["name"];
		header("Location: ../views/homePopularJury.php");
	}else{
		$msg = "User not found in system.";
		header("Location: ../views/signIn.php?msg=$msg");
	}
}

function professional($user){
	$u = new User();
	$boolean = $u->selectProfessional($user);
	if($boolean == true){
		session_start();
		$_SESSION["validated"] = "Professional";
		$_SESSION["name"] = $user["name"];
		header("Location: ../views/homeProfessionalJury.php");
	}else{
		$msg = "User not found in system.";
		header("Location: ../views/signIn.php?msg=$msg");
	}
}

function establishment($user){
	$u = new User();
	$boolean = $u->selectEstablishment($user);
	if($boolean == true){
		session_start();
		$_SESSION["validated"] = "Establishment";
		$_SESSION["name"] = $user["name"];
		header("Location: ../controllers/establishmentController.php?action=dataget");
	}else{
		$msg = "User not found in system.";
		header("Location: ../views/signIn.php?msg=$msg");
	}
}

function organizer($user){
	$u = new User();
	$boolean = $u->selectOrganizer($user);
	if($boolean == true){
		session_start();
		$_SESSION["validated"] = "Organizer";
		$_SESSION["name"] = $user["name"];
		header("Location: ../views/homeOrganizer.php");
	}else{
		$msg = "User not found in system.";
		header("Location: ../views/signIn.php?msg=$msg");
	}
}
?>
