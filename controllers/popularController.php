<?php
session_start();
require_once("../models/pincho.php");
require_once("../models/popular.php");
require_once("../models/popularValorations.php");
require_once("../models/codes.php");
require_once("../models/comments.php");
require_once("../models/establishment.php");
if(isset($_GET["action"])){
	$action = $_GET["action"];
	if($action == "logout"){
		session_start();
		$_SESSION["validated"]="";
		session_destroy();
		header("Location: ../index.php");
	}else if($action == "comments"){
		$p = new Pincho();
		$boolean = $p->selectAll();
		if($boolean == false){
			$msg = "Not validated pinchos yet in system.";
			header("Location: ../views/comments.php?msg=$msg");
		}else{
			$array = serialize($boolean);
			header("Location: ../views/comments.php?array=$array");
		}
	}else if($action == "gastromap"){
		getData();
	}else{
		echo("action no reconocida");
	}
}else if(isset($_POST["code1"])&&isset($_POST["code2"])&&isset($_POST["code3"])){
	$code1 = $_POST["code1"];
	$code2 = $_POST["code2"];
	$code3 = $_POST["code3"];
	$c = new Codes();
	$bool = $c->check($code1);
	if($bool == false){
		$msg = "Some code not found in system.";
		header("Location: ../views/popularVote.php?msg=$msg");
	}else{
		$idp1 = $bool[0]["Pincho_code"];
		$ide1 = $bool[0]["Pincho_Establishment_idEstablishment"];
		$idpop1 = $bool[0]["Popular_idPopular"];
		$bool = $c->check($code2);
		if($bool == false){
			$msg = "Some code not found in system.";
			header("Location: ../views/popularVote.php?msg=$msg");			
		}else{
			$idp2 = $bool[0]["Pincho_code"];
			$ide2 = $bool[0]["Pincho_Establishment_idEstablishment"];
			$idpop2 = $bool[0]["Popular_idPopular"];
			$bool = $c->check($code3);
			if($bool == false){
				$msg = "Some code not found in system.";
				header("Location: ../views/popularVote.php?msg=$msg");	
			}else{
				$idp3 = $bool[0]["Pincho_code"];
				$ide3 = $bool[0]["Pincho_Establishment_idEstablishment"];
				$idpop3 = $bool[0]["Popular_idPopular"];
				if($code1 == $code2 || $code1 == $code3 || $code2 == $code3){
					$msg = "Codes must be different.";
					header("Location: ../views/popularVote.php?msg=$msg");						
				}else{
					$pin = new Pincho();
					$pincho1 = serialize($pin->getbyCode($idp1));
					$pincho2 = serialize($pin->getbyCode($idp2));
					$pincho3 = serialize($pin->getbyCode($idp3));
					header("Location: ../views/popularVotePincho.php?pincho1=$pincho1&idp1=$idp1&ide1=$ide1&idpop1=$idpop1&pincho2=$pincho2&idp2=$idp2&ide2=$ide2&idpop2=$idpop2&pincho3=$pincho3&idp3=$idp3&ide3=$ide3&idpop3=$idpop3&code1=$code1&code2=$code2&code3=$code3");
				}	
			}			
		}
	}
}else if(isset($_POST["idp1"])){
	$idp1 = $_POST["idp1"];
	$ide1 = $_POST["ide1"];
	$idpop1 = $_POST["idpop1"];
	$code11 = $_POST["code11"];
	$code12 = $_POST["code12"];
	$code13 = $_POST["code13"];
	$pv = new PopularValorations();
	$boolean = $pv->insert($idp1,$ide1,$idpop1);
	if($boolean == false){
		echo("Database error: Try again");
	}else{
		//Borramos codigos para que no puedan ser reutilizados
		$cod = new Codes();
		$bool = $cod->setUsed($code11);
		$bool = $cod->setUsed($code12);
		$bool = $cod->setUsed($code13);
		$msg = "Votation Done.";
		header("Location: ../views/popularVote.php?msg=$msg");
	}
}else if(isset($_POST["name"]) && isset($_POST["pass"]) && isset($_POST["email"]) && isset($_POST["phone"])){
//Editar perfil populat
	$popular = array();
	$popular["name"] = $_POST["name"];
	$popular["pass"] = $_POST["pass"];
	$popular["email"] = $_POST["email"];
	$popular["phone"] = $_POST["phone"];
	$p = new Popular();
	$boolean = $p->isValid($popular);
	if($boolean == false){
		$msg = "User already exists";
		header("Location: ../views/signIn.php?msg=$msg");
        }else{
		$id = $_SESSION["name"];
		$bool = $p->update($popular,$id);
		if($bool == true){
			$_SESSION["name"] = $popular["name"];
			$msg = "Your profile was updated";
			header("Location: ../views/signIn.php?msg=$msg");
		}else{
			$msg = "Server error. Try again";
			header("Location: ../views/editProfileProfessional.php?msg=$msg");
		}	
        }
}else if(isset($_POST["idp2"])){
	$idp2 = $_POST["idp2"];
	$ide2 = $_POST["ide2"];
	$idpop2 = $_POST["idpop2"];
	$code21 = $_POST["code21"];
	$code22 = $_POST["code22"];
	$code23 = $_POST["code23"];
	$pv = new PopularValorations();
	$boolean = $pv->insert($idp2,$ide2,$idpop2);
	if($boolean == false){
		echo("Database error: Try again");
	}else{
		$cod = new Codes();
		$bool = $cod->setUsed($code21);
		$bool = $cod->setUsed($code22);
		$bool = $cod->setUsed($code23);
		$msg = "Votation Done.";
		header("Location: ../views/popularVote.php?msg=$msg");
	}	
}else if(isset($_POST["idp3"])){
	$idp3 = $_POST["idp3"];
	$ide3 = $_POST["ide3"];
	$idpop3 = $_POST["idpop3"];
	$code31 = $_POST["code31"];
	$code32 = $_POST["code32"];
	$code33 = $_POST["code33"];
	$pv = new PopularValorations();
	$boolean = $pv->insert($idp3,$ide3,$idpop3);
	if($boolean == false){
		echo("Database error: Try again");
	}else{
		$cod = new Codes();
		$bool = $cod->setUsed($code31);
		$bool = $cod->setUsed($code32);
		$bool = $cod->setUsed($code33);
		$msg = "Votation Done.";
		header("Location: ../views/popularVote.php?msg=$msg");
	}
}else if(isset($_GET["idpincho"])){//Código para leer comentarios
	$idpincho = $_GET["idpincho"];
	viewComments($idpincho);
}else if(isset($_POST["message"])){//Código para añadir comentario
	$message = $_POST["message"];
	$idpincho = $_POST["idpincho"];
	addComment($message,$idpincho);
}else{
	echo("No recibe");
}
function viewComments($idpincho){
	$c = new Comments();
	$array = $c->getComments($idpincho);
	if($array == false){
		$msg = "Not comments for this Pincho yet.";
		header("Location: ../views/commentsAdd.php?msg=$msg&idpincho=$idpincho");
	}else{
		$datos = array();
		$i = 0;
		foreach($array as $comment){
			$datos[$i][0] = $comment["Message"];
			$datos[$i][1] = $comment["Pincho_code"];
			$datos[$i][2] = $comment["Pincho_Establishment_idEstablishment"];
			$popid = $comment["Popular_idPopular"];
			$popular = new Popular();
			$popname = $popular->selectName($popid);
			$datos[$i][3] = $popname[0]["name"];
			$i = $i+1;
		}
		$arraysend = base64_encode(serialize($datos));
		header("Location: ../views/commentsAdd.php?arraysend=$arraysend&idpincho=$idpincho");
	}	
}
function addComment($message,$idpincho){
	session_start();
	$p = new Pincho();
	$pinfo = $p->getbyCode($idpincho);
	$idestablishment = $pinfo[0]["Establishment_idEstablishment"];
	$pop = new Popular();
	$popinfo = $pop->select($_SESSION["name"]);
	$idpopular = $popinfo[0]["idPopular"];
	$c = new Comments();
	$boolean = $c->add($message,$idpincho,$idestablishment,$idpopular);
	if($boolean == false){
		echo("Database error");
	}else{
		viewComments($idpincho);
	}
}

function getData()
{
		$e = new Establishment();
		$data = $e -> selectData();
		if($data == false){
			$msg = "Null";
			header("Location: ../views/gastroMap.php?msg=$msg");
		}else{
			$datos = array();
			$i = 0;
			foreach($data as $c){
				$datos[$i][1] = $c["name"];
				$datos[$i][2] = $c["coordenates"];
				$i = $i + 1;
			}
			$data = serialize($datos);
			header("Location: ../views/gastroMap.php?datos=$data");
		}
}
?>
