<?php
require_once("../models/professional.php");
require_once("../models/establishment.php");
require_once("../models/pincho.php");
require_once("../models/professionalValorations.php");
require_once("../models/popularValorations.php");
require_once("../models/popular.php");
if (isset($_POST["name"]) && isset($_POST["Email"]) && isset($_POST["Phone"]) && isset($_POST["Password"]) && isset($_POST["Passwordc"])) {
    $professional = array();
    $professional["name"] = $_POST["name"];
    $professional["email"] = $_POST["Email"];
    $professional["phone"] = $_POST["Phone"];
    $professional["password"] = $_POST["Password"];
    if ($_POST["Password"] == $_POST["Passwordc"]) {
        add($professional);
    } else {
        $msg = "Error: Password must be the same.";
        header("Location: ../views/createProfessional.php?msg=$msg");
    }

}else if(isset($_POST["proname"]) && isset($_POST["piname"])){
	$proname = $_POST["proname"];
	$piname = $_POST["piname"];
	pinAssignPro($proname,$piname);

}else if(isset($_POST["idE"])){//Borrado de establecimientos
	$idE = $_POST["idE"];
	$dbe = new Establishment();
	$dbp = new Pincho();
	$dbpop = new PopularValorations();
	$dbpro = new ProfessionalValorations();
	$borra = $dbpro->borradoEst($idE);
	$borra = $dbpop->borradoEst($idE);
	$borra = $dbp->borradoEst($idE);
	$borra = $dbe->borradoEst($idE);
	header("Location: organizerController.php?action=management");
}else if(isset($_POST["idP"])){//Borrado de pinchos
	$idP = $_POST["idP"];
	$dbp = new Pincho();
	$dbpop = new PopularValorations();
	$dbpro = new ProfessionalValorations();
	$borra = $dbpro->borradoPin($idP);
	$borra = $dbpop->borradoPin($idP);
	$borra = $dbp->borradoPin($idP);	
	header("Location: organizerController.php?action=management");
}else if(isset($_POST["idPo"])){//Borrado de populares
	$idPo = $_POST["idPo"];
	$dbp = new Popular();
	$dbpop = new PopularValorations();
	$borra = $dbpop->borradoPopular($idPo);
	$borra = $dbp->borradoPopular($idPo);
	header("Location: organizerController.php?action=management");
}else if(isset($_POST["idPro"])){//Borrado de profesionales
	$idPro = $_POST["idPro"];
	$dbp = new Professional();	
	$dbpro = new ProfessionalValorations();	
	$borra = $dbpro->borradoProfessional($idPro);
	$borra = $dbp->borradoProfessional($idPro);
	header("Location: organizerController.php?action=management");
}else if(isset($_POST["idWinner"])){//Selección de ganador
	$idWinner = $_POST["idWinner"];
	$p = new Pincho();
	$lose = $p->doAllLoser();
	$win = $p->doWinner($idWinner);
	if($win == false){
		echo("Error de la base de datos");
	}else{
		$msg = "Winner Selected.Contest done.";
		header("Location: ../views/homeOrganizer.php?msg=$msg");
	}
} else {
    $action = $_GET["action"];
    if($action == "logout"){
	session_start();
	$_SESSION["validated"] = "";
	session_destroy();
	header("Location: ../index.php");
    }else if($action == "evalidation"){
	$e = new Establishment();
	$boolean = $e->selectNotValidated();
	if($boolean == false){
		$msg = "Not establishments for validate in system at this moment.";
		header("Location: ../views/establishmentsValidation.php?msg=$msg");
	}else{
		$array = serialize($boolean);
		header("Location: ../views/establishmentsValidation.php?array=$array");
	}
    }else if($action == "pvalidation"){
	$p = new Pincho();
	$boolean = $p->selectNotValidated();
	if($boolean == false){
		$msg = "Not pinchos for validate in system at this moment.";
		header("Location: ../views/pinchosValidation.php?msg=$msg");
	}else{
		$array = serialize($boolean);
		header("Location: ../views/pinchosValidation.php?array=$array");
	}
    }else if($action == "pvalida"){
	$name = $_GET["name"];
	$p = new Pincho();
	$boolean = $p->validatePincho($name);
	if($boolean == false){
		$msg = "Server error. Try again.";
		header("Location: ../views/homeOrganizer.php?msg=$msg");
	}else{
		$msg = "Pincho was validated.";
		header("Location: ../views/homeOrganizer.php?msg=$msg");
	}
    }else if($action == "pborra"){
	//A implementar en el futuro. Borrado del pincho
    }else if($action == "evalida"){
	$name = $_GET["name"];
	$e = new Establishment();
	$boolean = $e->validateEstablishment($name);
	if($boolean == false){
		$msg = "Server error. Try again.";
		header("Location: ../views/homeOrganizer.php?msg=$msg");
	}else{
		$msg = "Establishment was validated.";
		header("Location: ../views/homeOrganizer.php?msg=$msg");
	}
    }else if($action == "eborra"){
	//A implementar en el futuro. Borrado del establecimiento.
    }else if($action == "assign"){
		header("Location: ../views/pinAssigPro.php");
    }else if($action == "management"){
		$pro = new Professional();
		$pop = new Popular();
		$est = new Establishment();
		$pin = new Pincho();
		$establishments = serialize($est->selectAll());
		$pinchos = serialize($pin->selectnAll());
		$popular = serialize($pop->selectAll());
		$professional = serialize($pro->selectAll());
		header("Location: ../views/management.php?establishments=$establishments&pinchos=$pinchos&popular=$popular&professional=$professional");
    }else if($action == "winner"){//Seleccion de ganador del concurso
	$p = new Pincho();
	$finalistas = $p->selectFinalists();
	if($finalistas == false){
		$msg = "Not finalists selected by professional jury yet.";
		header("Location: ../views/selectWinner.php?msg=$msg");
	}else{
		$arrayD = array();
		$l = new PopularValorations();
		$v = new ProfessionalValorations();
		$i=0;//variable auxiliar
		foreach($finalistas as $finalista){
			$likes = $l->getRows($finalista["code"]);//Likes recibidos
			$valoration = $v->getVote($finalista["code"]);
			$val = $valoration[0]["Calification"];//Nota del profesional
			$name = $finalista["name"];
			$id = $finalista["code"];
			$arrayD[$i][0] = $name;
			$arrayD[$i][1] = $likes;
			$arrayD[$i][2] = $val;
			$arrayD[$i][3] = $id;
			$i++;
		}
		$data = serialize($arrayD);
		header("Location: ../views/selectWinner.php?data=$data");
	}
    }else{
	echo("No deberías entrar aquí.");
    }
}

function add($professional) {
    $p = new Professional();
    $boolean = $p->isValid($professional);
    if ($boolean == true) {
        $bool = $p->addPro($professional);
        if ($bool == true) {
            $msg = "Professional Jury added.";
            header("Location: ../views/createProfessional.php?msg=$msg");
        } else {
            $msg = "Error in database. Try again.";
            header("Location: ../views/createProfessional.php?msg=$msg");
        }
    } else {
        $msg = "Error: User Already exists.";
        header("Location: ../views/createProfessional.php?msg=$msg");
    }
}

//Funcion de asignación de pincho al profesional
function pinAssignPro($proname,$piname){
	//Recoger id del professional
	$p = new Professional();
	$boolean = $p->getId($proname);
	if($boolean == false){
		$msg = "Error: Professinal not found in system";
		header("Location: ../views/pinAssigPro.php?msg=$msg");	
	}else{
		$idProfessional = $boolean[0]["idProfessional"];
		$pi = new Pincho();
		$bool = $pi->getData($piname);
		if($bool == false){
		$msg = "Error: Pincho not found in system";
		header("Location: ../views/pinAssigPro.php?msg=$msg");
		}else{
			$idPincho = $bool[0]["code"];
			$idEstablishment = $bool[0]["Establishment_idEstablishment"];
			$v = new ProfessionalValorations();
			$bool2 = $v->isValid($idPincho);
			if($bool2 == false){
				$msg = "Error: Selected Pincho is actually assigned to Professional";
				header("Location: ../views/pinAssigPro.php?msg=$msg");
			}else{
				$insert = $v->insert($idPincho,$idProfessional,$idEstablishment);
				if($insert == false){
					$msg = "Internal database Error.Please, try again";
					header("Location: ../views/pinAssigPro.php?msg=$msg");
				}else{
					$msg = "Pincho assigned to Professional. Operation Done.";
					header("Location: ../views/pinAssigPro.php?msg=$msg");
				}
			}
		}
	}
}

?>
