<?php
session_start();
require_once("../models/professionalValorations.php");
require_once("../models/professional.php");
require_once("../models/pincho.php");
if(isset($_GET["action"])){
	$action = $_GET["action"];
	if($action == "logout"){
		session_start();
		$_SESSION["validated"]="";
		session_destroy();
		header("Location: ../index.php");
	}else if($action == "vote"){
		session_start();
		$name = $_SESSION["name"];
		$p = new Professional();
		$aux = $p->getId($name);
		$idPro = $aux[0]["idProfessional"];
		$v = new ProfessionalValorations();
		$boolean = $v->getAssigned($idPro);
		if($boolean == false){
			$msg = "You havent assigned pinchos yet.";
			header("Location: ../views/professionalVote.php?msg=$msg");
		}else{
			$arrayPinchos = array();
			$i=0;
			$pin = new Pincho();
			foreach($boolean as $pcode){
				$code = $pcode["Pincho_code"];
				$pincho = $pin->getbyCode($code);
				$arrayPinchos[$i] = $pincho;
				$i++;		
			}
			$pinchos = serialize($arrayPinchos);
			header("Location: ../views/professionalVote.php?pinchos=$pinchos");			
		}
	}else if($action == "finalist"){
		session_start();
		$name = $_SESSION["name"];
		$p = new Professional();
		$aux = $p->getId($name);
		$idPro = $aux[0]["idProfessional"];
		$v = new ProfessionalValorations();
		$boolean = $v->getAssigned($idPro);
		if($boolean == false){
			$msg = "You havent assigned pinchos yet.";
			header("Location: ../views/professionalFinalist.php?msg=$msg");
		}else{
			$arrayPinchos = array();
			$i=0;
			$pin = new Pincho();
			foreach($boolean as $pcode){
				$code = $pcode["Pincho_code"];
				$pincho = $pin->getbyCode($code);
				$arrayPinchos[$i] = $pincho;
				$i++;		
			}
			$pinchos = serialize($arrayPinchos);
			header("Location: ../views/professionalFinalist.php?pinchos=$pinchos");
		}
	}else{
		echo("No deberías estar aquí.");
	}
}else if(isset($_POST["votation"]) && isset($_POST["idPincho"])){
	$nota = $_POST["votation"];
	$idPincho = $_POST["idPincho"];
	$pval = new ProfessionalValorations();
	$boolean = $pval->doVotation($nota,$idPincho);
	if($boolean == false){
		$msg = "Database update error";
		header("Location: ../views/professionalVote.php?msg=$msg");
	}else{
		header("Location: professionalController.php?action=vote");
	}

}else if(isset($_POST["idFinalist"])){//Marcar pincho como finalista
	$idPincho = $_POST["idFinalist"];
	$p = new Pincho();
	$bool = $p->setFinalist($idPincho);
	if($bool == false){
		$msg = "Database update error";
		header("Location: ../views/professionalFinalist.php?msg=$msg");
	}else{
		header("Location: professionalController.php?action=finalist");
	}
	
}else if(isset($_POST["name"]) && isset($_POST["pass"]) && isset($_POST["email"]) && isset($_POST["phone"])){
//Editar perfil profesional
	$professional = array();
	$professional["name"] = $_POST["name"];
	$professional["pass"] = $_POST["pass"];
	$professional["email"] = $_POST["email"];
	$professional["phone"] = $_POST["phone"];
	$p = new Professional();
	$boolean = $p->isValid($professional);
	if($boolean == false){
		$msg = "User already exists";
		header("Location: ../views/editProfileProfessional.php?msg=$msg");
        }else{
		$id = $_SESSION["name"];
		$bool = $p->update($professional,$id);
		if($bool == true){
			$_SESSION["name"] = $professional["name"];
			$msg = "Your profile was updated";
			header("Location: ../views/editProfileProfessional.php?msg=$msg");
		}else{
			$msg = "Server error. Try again";
			header("Location: ../views/editProfileProfessional.php?msg=$msg");
		}	
        }
	
}else{
echo("No recibe la acción");
}
?>
