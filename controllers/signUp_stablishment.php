<?php

require_once("../models/establishment.php");

if ((isset($_POST["name"])) && (isset($_POST["email"])) && isset($_POST["password"])){

    $establishment = array();
    $establishment["name"] = $_POST["name"];
    $establishment["coordenates"] = $_POST["coordenates"];
    $establishment["phone"] = $_POST["phone"];
    $establishment["email"] = $_POST["email"];
    $establishment["schedule"] = $_POST["schedule"];
    $establishment["webpage"] = $_POST["webpage"];
    $establishment["password"] = $_POST["password"];

    $es = new Establishment();
    $boolean = $es->exists($establishment);
    if($boolean == false){
	$msg = "Establishment name already exists in system.";
	header("Location: ../views/signUpEstablishment.php?msg=$msg");
    }else{
    	$es->insertEstablishment($establishment);
    	header("Location: ../views/signIn.php");
    }
    
  } else {

    header("Location: ../views/signUpEstablishment.php");
}
?>
