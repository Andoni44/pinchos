<?php

require_once("../models/user.php");

if (isset($_POST["name"]) && isset($_POST["password"]) && isset($_POST["phone"]) && isset($_POST["email"])) {
    $popular = array();
    $popular["name"] = $_POST["name"];
    $popular["password"] = $_POST["password"];
    $popular["phone"] = $_POST["phone"];
    $popular["email"] = $_POST["email"];

    $user = new User();
    $boolean = $user->exists($popular);
    if($boolean == false){
	$msg = "Popular name already exists in system.";
	header("Location: ../views/signUpPopular.php?msg=$msg");
    }else{
    	$user->insertPopular($popular);
   	 header("Location: ../views/signIn.php");
    }
} else {
    header("Location: ../views/signUpPopular.php");
}
?>
