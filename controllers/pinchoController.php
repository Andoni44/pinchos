<?php
session_start();
require_once("../models/pincho.php");
require_once("../models/establishment.php");

/* Comprobamos si los datos llegan del formulario de registro de pinchos */
if (isset($_POST["name"]) && isset($_POST["description"]) && isset($_POST["price"]) && isset($_POST["ingredients"]) && isset($_POST["url"])) {
    $pincho = array();
    $pincho["name"] = $_POST["name"];
    $pincho["description"] = $_POST["description"];
    $pincho["price"] = $_POST["price"];
    $pincho["ingredients"] = $_POST["ingredients"];
    $pincho["url"] = $_POST["url"];
    $aux = new Establishment();
    $name = $_SESSION["name"];
    $bool = $aux->select($name);
    $pincho["idEstablishment"] = $bool[0]["idEstablishment"]; /* Aqui se pillaría la sesion actual con la id del establecimiento */
    createPincho($pincho);
} else {
    $action = $_GET["action"];
    if ($action == "view") {
        viewPincho();
    } else {
        //Procesamiento de futuras peticiones que vengan por GET
        echo("No recibe los datos del GET");
    }
}

function createPincho($pincho) {
    $p = new Pincho();
    $boolean = $p->isValid($pincho);
    if ($boolean == false) {
        $a = serialize("Los datos del formulario no han sido validados.");
        header("Location: ../views/requestManagement.php?b=$a");
    } else {
        $bool = $p->insert($pincho);
        if ($bool == true) {
            $a = serialize("La solicitud se ha enviado correctamente.");
            header("Location: ../views/requestManagement.php?c=$a");
        } else {
            $a = serialize("Se ha producido un error en la base de datos.");
            header("Location: ../views/requestManagement.php?d=$a");
            ;
        }
    }
}

function viewPincho() {
    $aux = new Establishment();
    $name = $_SESSION["name"];
    $bool = $aux->select($name);
    $id = $bool[0]["idEstablishment"];
    $p = new Pincho();
    $boolean = $p->select($id);
    if ($boolean == false) {
        header("Location: ../views/notPinchoEstablishment.php");
    } else {
        $name = $boolean["0"]["name"];
        $url = $boolean["0"]["url"];
        $description = $boolean["0"]["description"];
        $ingredients = $boolean["0"]["ingredients"];
        $price = $boolean["0"]["price"];
        header("Location: ../views/viewPinchoEstablishment.php?name=$name&url=$url&description=$description&ingredients=$ingredients&price=$price");
    }
}

?>
