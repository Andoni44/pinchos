<?php
require_once("../models/pincho.php");
require_once("../models/popularValorations.php");
require_once("../models/professionalValorations.php");
if(isset($_GET["id"])){
	$id = $_GET["id"];
	$name = $_GET["name"];
	$description = $_GET["description"];
	$price =  $_GET["price"];
	$ingredients = $_GET["ingredients"];
	$url = $_GET["url"];
	$p = new PopularValorations();
	$likes = $p->getRows($id);
	$pro = new ProfessionalValorations();
	$boolean = $pro->getVote($id);
	if($boolean == false){
		$msg = "Selected pincho havent a professional jury assigned yet.";
		header("Location: ../views/pinchoInfo.php?msg=$msg");
	}else{
		$nota = $boolean[0]["Calification"];
		header("Location: ../views/pinchoInfo.php?name=$name&description=$description&price=$price&ingredients=$ingredients&url=$url&likes=$likes&nota=$nota");
	}
	
}else if($_GET["action"]){
	$action = $_GET["action"];
	if($action == "finalists"){//Mostrar galeria de finalistas
		$p = new Pincho();
		$boolean = $p->selectFinalists();
		if($boolean == false){
			$msg = "Not finalist pinchos yet in system.";
			header("Location: ../views/gallery.php?msg=$msg");
		}else{
			$array = serialize($boolean);
			header("Location: ../views/gallery.php?array=$array");
		}
	}else if($action == "winner"){//Mostrar pincho ganador
		$p = new Pincho();
		$boolean = $p->selectWinner();
		if($boolean == false){
			$msg = "Not winner yet. Vote your favourite pinchos!!!";
			header("Location: ../views/gallery.php?msg=$msg");			
		}else{
			$array = serialize($boolean);
			header("Location: ../views/gallery.php?array=$array");			
		}
	}else{

	}
}else{
	$p = new Pincho();
	$boolean = $p->selectAll();
	if($boolean == false){
		$msg = "Not validated pinchos yet in system.";
		header("Location: ../views/gallery.php?msg=$msg");
	}else{
		$array = serialize($boolean);
		header("Location: ../views/gallery.php?array=$array");
	}
}
?>
